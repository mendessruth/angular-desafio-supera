# Empleados

Este projeto foi criado com angular cli.

## Servidor de desenvolvimento

Execute `ng serve` para um servidor dev. Navegue até `http://localhost:4200/`. O aplicativo será recarregado automaticamente se você alterar qualquer um dos arquivos de origem.

## Build

Execute `ng build` para compilar o projeto. Os artefatos de construção serão armazenados no diretório `dist/`. Use o sinalizador `--prod` para uma compilação de produção.

## Executando testes de unidade

Execute `ng test` para executar os testes de unidade.

## Executando testes de ponta a ponta

Execute `ng e2e` para executar os testes de ponta a ponta.

## Mais ajuda

Para obter mais ajuda na CLI do Angular, use `ng help`.
